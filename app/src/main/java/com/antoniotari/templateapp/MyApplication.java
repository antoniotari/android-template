package com.antoniotari.templateapp;

import com.antoniotari.android.jedi.JediUtil;
import com.antoniotari.templateapp.injection.MyModule;
import com.facebook.drawee.backends.pipeline.Fresco;

import android.app.Application;

/**
 * Created by anthony on 9/19/15.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        JediUtil.init(this, new MyModule(this));
        Fresco.initialize(this);
    }
}