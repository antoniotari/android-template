package com.antoniotari.templateapp.adapters;

import com.antoniotari.templateapp.R;
import com.antoniotari.templateapp.adapters.DrawerAdapter.ViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;

import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by anthony on 9/19/15.
 */
public class DrawerAdapter extends RecyclerView.Adapter<ViewHolder> {

    public interface OnItemClickListener{
        void onClick(int position,String title,View view);
    }

    private List<String> mTitles;
    private OnItemClickListener mOnClickListener;

    Uri[] urls={Uri.parse("http://digiflare.s3.amazonaws.com/vdms/android/images/splash_logo.png")
            ,Uri.parse("http://digiflare.s3.amazonaws.com/vdms/android/images/nav_icon_featured.png")
            ,Uri.parse("http://digiflare.s3.amazonaws.com/vdms/android/images/nav_icon_programguide.png")
            ,Uri.parse("http://digiflare.s3.amazonaws.com/vdms/android/images/nav_icon_favourites.png")
            ,Uri.parse("http://digiflare.s3.amazonaws.com/vdms/android/images/nav_icon_settings.png")};

    public DrawerAdapter(List<String> titles){
        mTitles=titles;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_card, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    public void setOnClickListener(OnItemClickListener listener){
        mOnClickListener=listener;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.personName.setText(mTitles.get(position));
        holder.cv.setOnClickListener(v -> mOnClickListener.onClick(position, mTitles.get(position),v));
        holder.personPhoto.setImageURI(urls[position]);
        holder.personPhoto.setOnClickListener(v->remove(mTitles.get(position)));
        holder.personAge.setText(String.valueOf(position));
    }

    public void add(int position, String item) {
        mTitles.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(String item) {
        int position = mTitles.indexOf(item);
        mTitles.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return mTitles.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.cv)CardView cv;
        @Bind(R.id.person_name)TextView personName;
        @Bind(R.id.person_age)TextView personAge;
        @Bind(R.id.person_photo)SimpleDraweeView personPhoto;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
