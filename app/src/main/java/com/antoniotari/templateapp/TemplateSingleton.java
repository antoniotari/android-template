package com.antoniotari.templateapp;

import com.antoniotari.android.jedi.JediUtil;

import android.content.Context;

/**
 * Created by anthony on 9/19/15.
 */
public class TemplateSingleton {

    private Context mApplicationContext;

    private static int sInstanceCounter=0;

    public TemplateSingleton(Context context) {
        //Log.hi("blublu constructor");
        JediUtil.inject(this);
        if(++sInstanceCounter > 1)throw new RuntimeException("this is a singleton, please inject or use getInstance");
        mApplicationContext = context;
    }

    public static TemplateSingleton getInstance() {
        return JediUtil.fromInject(TemplateSingleton.class); //ApplicationGraph.getObjectGraph().get(TemplateSingleton.class);
    }

    public String getPackageName(){
        return mApplicationContext.getPackageName();
    }
}
